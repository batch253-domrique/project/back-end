db.users.insertMany([
        {
            firstName: "Stephen",
            lastName: "Hawking",
            age: 76,
            contact: {
                phone: "87654321",
                email: "stephenhawking@mail.com"
            },
            courses: ["Python", "React", "PHP", "CSS"],
            department: "none"
        },
        { 
            firstName: "Neil",
            lastName: "Armstrong",
            age: 82,
            contact: {
               phone: "98765432",
               email: "neilarmstrong@mail.com"
            },
            courses: ["React", "Laravel", "Sass"],
            department: "none"            
        }
    ]);
 db.users.find({"firstName": "Stephen"});