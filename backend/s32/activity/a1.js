let http = require("http");
let port = 4000;

http.createServer((request, response) => {
    if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Welcome to Booking System");
	}else if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Welcome to your profile!");
	}else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Here’s our courses available");
	}else if(request.url == "/addcourse" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Add a course to our resources");
	}else if(request.url == "/updatecourse" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Update a course to our resources");
	}else if(request.url == "/archivecourse" && request.method == "GET"){
		response.writeHead(200, {"Content-type": "text/plain"});
		response.end("Archive courses to our resources");
	}

}).listen(4000);

console.log(`Server running at localhost:${port}`);