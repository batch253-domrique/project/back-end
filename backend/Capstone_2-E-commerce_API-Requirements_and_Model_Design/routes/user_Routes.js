const express = require("express");
const router = express.Router();
const userController = require("../controllers/user_Controller");
const auth = require("../auth");

// ROUTES REGISTRATION_________________________________________________________________
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

// ROUTES AUTHENTICATION_________________________________________________________________
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

//Do not touch this exports.............................
module.exports = router;
