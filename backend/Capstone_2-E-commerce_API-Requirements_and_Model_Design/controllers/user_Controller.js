const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User_models");
// const Course = require("../models/Course");

// USER REGISTRATION_________________________________________________________________
//redo => "true" for line20 soon...
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser
    .save()
    .then((user) => {
      if (user) {
        return `Hi ${reqBody.firstName}, you are now registered!`;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

// USER AUTHENTICATION_________________________________________________________________
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return false;
        }
      }
    })
    .catch((err) => err);
};
