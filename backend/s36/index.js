//#1. Set-up dependencies
const express = require("express");
const mongoose = require("mongoose");

//#6. Connect the route
// This allows us to use all the routes defined in "taskRoute.js"
taskRoute = require("./routes/taskRoute");

//2. Server set-up
const app = express();
const port = 4000;
//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true})); // to read url in code

//3. Connect Mongoose : Database connection
//4. Add parser right after URL
mongoose.connect("mongodb+srv://admin:admin123@batch253-domrique.wk1bqpw.mongodb.net/s36?retryWrites=true&w=majority",
    {
        useNewUrlParser : true, //we need new parser to not the old one, so we use this
        useUnifiedTopology: true //reroute server if the server is down
    }
);
//#8 Connect MongoDB
let db = mongoose.connection; 
// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

//#7 add a route named /tasks
app.use("/tasks", taskRoute)


//5. Listen port
if(require.main === module){
    app.listen(port, ()=> console.log(`Server running at ${port}`));
}

module.exports = app;

