//this is a model module
//since we need schema, dependencies : mongoose

//#1. Create the Schema, model and export the file
const mongoose = require("mongoose");

//#2. Create schema
const taskSchema = new mongoose.Schema({

    name: String,
    status:{
        type: String,
        default: "pending"
    }
});

//#4. Create a model
module.exports = mongoose.model("Task", taskSchema);

