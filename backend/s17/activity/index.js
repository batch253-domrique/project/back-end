/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here: 
	function printUserInfo() {
		fullName = "John Doe";
		currentAge = 25;
		myAddress = "123 street, Quezon City";
		myCat = "Joe";
		myDog = "Danny";

		let greetsName = "Hello, I'm " + fullName;
		let stateAge = "I am " + currentAge + " years old.";
		let stateAddress = "I live in " + myAddress + ".";
		let yourPet = "I have a cat named " + myCat + ".";
		let yourDog = "I have a dog named " + myDog + ".";

		console.log("printUserInfo()");
		console.log(greetsName);
		console.log(stateAge);
		console.log(stateAddress);
		console.log(yourPet);
		console.log(yourDog);
	}


	printUserInfo();

/*

	2. Create a function named printFiveBands which is able to display 
		5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	function printFiveBands() {
		let bandA = "The Beatles" ;
		let bandB = "Taylor Swift";
		let bandC = "The Eagles";
		let bandD = "Rivermaya";
		let bandE = "Eraserheads";
		
		console.log("printFiveBands()");
		console.log(bandA);
		console.log(bandB);
		console.log(bandC);
		console.log(bandD);
		console.log(bandE);
	}

	printFiveBands();
/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	function printFiveMovies() {
		let movieA ="Lion King" ;
		let movieB ="Howl's Moving Castle" ;
		let movieC ="Meet the Robinsons" ;
		let movieD ="School of Rock" ;
		let movieE ="Spirited Away" ;

		console.log("printFiveMovies()");
		console.log(movieA);
		console.log(movieB);
		console.log(movieC);
		console.log(movieD);
		console.log(movieE);
	}

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("printFriends()");
	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

//printFriends
printFriends();

// console.log(friend1);
// console.log(friend2);








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printMyFiveBands,
		printMyFiveMovies,
		printFriends
	}
} catch(err){

}