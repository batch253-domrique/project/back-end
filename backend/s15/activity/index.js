console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	let fullName;

	fullName = "Jhun Robb Domrique";
	let firstName = "Jhun Robb";
	let lastName = "Domrique";
	let currentAge = 20;
	let hobbies = ["singing", "surfing", "media streaming"];
	let workAddress = {
		houseNumber: '12',
		street: "Bravo st.",
		city: "Cabanatuan City",
		state: "Central Luzon",
	};
	console.log("First Name: " + firstName)
	console.log("Last Name: " + lastName)
	console.log("Age: " + currentAge)
	console.log("Hobbies : ");
	console.log(hobbies);
	console.log("Work Address: ");
	console.log(workAddress);

	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Natalia","Johnson","Aamon","Baxia","Wanwan","Gord"];
	console.log("My Friends are: " + friends)
	console.log(friends);

	let profile = {

		username: "robb_gmail",
		fullName: "Jhun Robb Domrique",
		age: 20,
		isActive: false,

	};
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Mister Google";
	console.log("My bestfriend is: " + fullName);


	const lastLocation = "Arctic Ocean";

	console.log("I was found frozen in: " + lastLocation);

	//Do not modify
    //For exporting to test.js
    try{
        module.exports = {
            firstName, lastName, age, hobbies, workAddress,
            fullName, currentAge, friends, profile, fullName2, lastLocation
        }
    } catch(err){
    }