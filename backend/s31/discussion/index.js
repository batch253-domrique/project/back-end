// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
// The messages sent by the client, usually a Web browser, are called requests
// The messages sent by the server as an answer are called responses.
// An overview of HTTP ====> https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

//para mag load ang hhtp module (http module for transfer data)
let http = require("http"); 

//need to use this http module to use the createServer
//magigigng laman ni Create server yung request at response.
http.createServer(function(request, response){

		//HTTP Response Status Codes === https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
		// Use the writeHead() method to:
	    // Set a status code for the response - a 200 means OK
	    // Set the content-type of the response as a plain text message
		/*	Informational responses (100 – 199)
			Successful responses (200 – 299)
			Redirection messages (300 – 399)
			Client error responses (400 – 499)
			Server error responses (500 – 599)
		*/
	response.writeHead(200, {"Content-Type": "text/plain"});

		// Send the response with text content 'Hello World'
	response.end("Hello World");

		// A port is a virtual point where network connections start and end.
		// Each port is associated with a specific process or service
		// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server
}).listen(4000);

console.log("Server running at localhost:4000");

//==================================================================================
//Sample set-up

// let http = require("http"); 

// http.createServer(function(request, response){
// 	response.writeHead(200, {"Content-Type": "text/plain"});
// 	response.end("Hello World");
// 	}).listen(4000);

// console.log("Server running at localhost:4000");

//=================================================================================
//Actual Sample

// const http = require("http");
// const port = 4000;
// const server = http.createServer((request, response) => {
// 	if(request.url == "/greeting"){
// 		response.writeHead(200, {"Content-Type": "text/plain"});
// 		response.end("Hello again!")
// 	}else if(request.url == "/homepage"){
// 		response.writeHead(200, {"Content-Type": "text/plain"});
// 		response.end("This is the homepage");
// 	}else{
// 		response.writeHead(404, {"Content-Type": "text/plain"});
// 		response.end("Page not available");
// 	}
// });
// server.listen(port);
// console.log(`Server now accessible at localhost:${port}.`)
