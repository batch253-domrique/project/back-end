const bcrypt = require("bcrypt");
const auth = require("../auth");

const Course = require("../models/Course");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody) => {
  // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
  // Uses the information from the request body to provide all the necessary information
  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  });

  return newCourse
    .save()
    .then((course) => true)
    .catch((err) => false);
};

//+++++++++++++++++++++ACTIVITY SEASON 39 START ++++++++++++++++++++++++++++++//
module.exports.addCourse = (reqBody, admin) => {
  if (admin) {
    let newCourse = new Course({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });
    return newCourse
      .save()
      .then((course) => true)
      .catch((err) => false);
  } else {
    return Promise.reject(false);
  }
};
/*module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return err
                } else {
                    //course creation successful
                    return true;
                }
            })
        }
        
    });    
}*/

//+++++++++++++++++++++ACTIVITY SEASON 39 END++++++++++++++++++++++++++++++++//

//+++++++++++++++++++++ACTIVITY SEASON 40 START ++++++++++++++++++++++++++++++//
//Retrieve for retrieving all the courses
/*
    Steps:
    1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {
  return Course.find()
    .then((result) => result)
    .catch((err) => err);
};

//mini_activity
// http://localhost:4000/courses
// http://localhost:4000/courses/
module.exports.getAllActive = () => {
  return Course.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

//mini-activity 2
//Retrieve the course that matches the course ID provided deom the URL
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId)
    .then((result) => result)
    .catch((err) => err);
};

// Update a course
/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (reqParams, reqBody) => {
  // Specify the fields/properties of the document to be updated
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(reqParams.id, updatedCourse)
    .then((course) => true)
    .catch((err) => err);
};

/*
    Syntax:
      ModelName.findByIdAndUpdate(documentIs, updatesToBeApplied).then(statement)

      req.params = {courseId : 633fdgdgdg566dghdt647kln5jnjkn5}
      req.params.id = 633fdgdgdg566dghdt647kln5jnjkn5
*/

// Route to archiving a course
// A "PUT"/"PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
//=============Activity s40+++++++++++++++++++++++++++++++++++++++++++++//

module.exports.patchCourse = (reqParams) => {
  let updatedCourse = {
    isActive: false,
  };
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then((course) => true)
    .catch((err) => err);
};

/* module.exports.archiveCourse = (reqParams) => {
  let updateActiveField = {
    isActive: false,
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField)
    .then((course) => true)
    .catch((err) => err);
}; */
