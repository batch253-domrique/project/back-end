

let trainer= {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle","Bulbasaur"],
	Friends: {
		hoenn:["May","Max"],
		kanto:["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
};
console.log(trainer);
//dot notation
console.log("Result of dot notation:")
console.log(trainer.Name);
//square bracket notation
console.log("Result of square bracket notation:")
console.log(trainer["Pokemon"]);

console.log("Result of talk method");
trainer.talk();



function Pokemon(Name, Level){
	this.Name = Name,
	this.Level = Level,
	this.Health =2 * Level,
	this.Attack = Level
	

	
	//Methods
	this.tackle = function(target){
		if ([target.Health >= 0]) {
			console.log(this.Name + " tackled " + target.Name)
			console.log(target.Name + "'s health is now reduced to " + [target.Health - this.Attack])
		}if([target.Health <= 0]){
			console.log(target.Name + " fainted.");
		}
	}
}
let Pikachu = new Pokemon("Pikachu", 12);
console.log(Pikachu);
let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude);
let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
	
console.log(Pikachu);
Mewtwo.tackle(Geodude);

console.log(Geodude);


Pikachu.tackle(Geodude);








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}