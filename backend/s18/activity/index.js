// console.log("Hello World") pi=  3.1416

function addNum(addNum1, addNum2){
	result = addNum1 + addNum2
	console.log("Displayed sum of " + addNum1 + " and " + addNum2);
	console.log(result)
}

addNum(5,5);

function subNum(subNum1, subNum2){
	result = subNum1 - subNum2
	console.log("Displayed the difference " + subNum1 + " and " + subNum2);
	console.log(result)
}

subNum(20,5);

// function multiplyNum(multNum1, multNum2){
// 	let result = multNum1 * multNum2
// 	// "The product of " + multNum1 + " and " + multNum2 + ":");
// 	return result;
// }
// let resultStatement = multiplyNum(50,10);
// console.log(resultStatements)

function multiplyNum(numA, numB){
	let result = numA * numB;
	return result;
}

let product = multiplyNum(50,10);
console.log("The product of 50 and 10:");
console.log(product);

function divideNum(numA, numB){
	let result = numA / numB;
	return result;
}

let quotient = divideNum(50,10);
console.log("The quotient of 50 and 10:");
console.log(quotient);


function getCircleArea(radius){
	let areaCircle = 3.1416 * (radius ** 2);
	return areaCircle;
}

let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);


function getAverage(totalAve){
	let average = (20 + 40 + 60 + 80)/4;
	return average;
}

let averageVar = getAverage();
console.log("The average of 20, 40, 60 and 80:");
console.log(averageVar);

function checkIfPassed(){
	let yourScore = 38;
	let totalScore = 50;
	let scorePercentage = (yourScore / totalScore) * 100;
	let isPassed= scorePercentage >= 75;
	return isPassed;
}

let isPassingScore = checkIfPassed(38,50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);



















//Do not modify
//For exporting to test.js

try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}