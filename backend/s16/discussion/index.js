// console.log("Hello");

// [SECTION] Arithmetic Operators
let x = 123, y= 456;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y; /* asterisk for multiplication */
console.log("Result of multiplication operator: " + product);

let division = x - y;  /* forward slash for division */
console.log("Result of division operator: " + division);

let remainder = x % y; /* percent for remainder*/
console.log("Result of modulo operator " + remainder);

let assignmentNumber = 8;

// Addition Assignment Operator
// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2; //shorthand
console.log("Result of addition assignment operator: " + assignmentNumber);

/*
	Mini-Activity:
		1. Use assignment operators to other arithmetic operators.
		2. Console log the results
		3. Send screenshots to our Hangouts. (code and output)
*/

assignmentNumber += 2; //shorthand
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2; //shorthand
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2; //shorthand
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2; //shorthand
console.log("Result of division assignment operator: " + assignmentNumber);

assignmentNumber %= 2; //shorthand
console.log("Result of modulo assignment operator: " + assignmentNumber);


/*
    JS follows the PEMDAS rule:
    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    3. 1 + 2 = 3
    4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pmdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pmda: " + pmdas);

//Increment and Decrement
/*Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied*/
let z = 1;
//let increment = ++z;
let increment = ++z; //pre-increment (no need reassignment)
console.log("Result of  pre-increment: " + increment);
console.log("Result of  pre-increment: " + z);

increment = z++; //post-increment
console.log("Result of  post-increment: " + increment);
console.log("Result of  post-increment: " + z);

let decrement = --z; //pre-decrement
console.log("Result of  pre-decrement: " + decrement);
console.log("Result of  pre-decrement: " + z);

decrement = z--; //post-decrement
console.log("Result of  post-decrement: " + decrement);
console.log("Result of  post-decrement: " + z);

// [SECTION] Type Coercion

/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16, numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

//[SECTION] Comparison Operators

//
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

// compare the value (regardless the data type)
let juan = 'juan';
console.log(1 == 1);//true
console.log(1 == 2);//false
console.log(1 == '1');//true
console.log("juan" == "juan");//true
console.log(juan == "juan");//true

// Inequality operator (!=)
/* 
    - Checks whether the operands are not equal/have different content
    - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);//false
console.log(1 != 2);//true
console.log(1 != '1');//false
console.log("juan" != "juan");//false
console.log(juan != "juan");//false


// Strict Equality Operator (===)
/* 
    - Checks whether the operanys are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to
	- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/
// compare data-type and value
console.log(1 === 1);//true
console.log(1 === 2);//false
console.log(1 === '1');//false
console.log("juan" === "juan");//true
console.log(juan === "juan");//true

//Strict Inequality Operator
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/

console.log(1 !== 1);//false
console.log(1 !== 2);//true
console.log(1 !== '1');//true
console.log("juan" !== "juan");//false
console.log(juan !== "juan");//false

//Relational Operators
//Some comparison operators check whether one value is greater or less to the other value.
//Boolean value
let a = 50;
let b = 65;

//GT (>)
let isGreaterThan = a > b; //false
//LT (<)
let isLessThan = a < b; //true
//GTE (>=)
let isGTorEqual = a >= b; //false
//GTE (<=)
let isLTorEqual = a <= b; //true

console.log(isGreaterThan); //false
console.log(isLessThan); //true
console.log(isGTorEqual); //false
console.log(isLTorEqual); //true

console.log(a > '30'); //forced coercion to change the string to a number //true
console.log(a > 'twenty'); //false

let str ='forty';
console.log(b >= str);//Nan - not a number

//[SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

//Logical AND operator (&& - double ampersand)
//Returns true if all operands are true (1 * 0 = 0)
let areAllRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + areAllRequirementsMet)

//Logical OR operator (|| - double pipe)
//Returns true if one of the operands are true (1 + 0 = 1)
let areSomeAllRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + areSomeAllRequirementsMet)

// Logical NOT Operator (! - exclamation point)
let areSomeAllRequirementsMetNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + areSomeAllRequirementsMetNotMet);

