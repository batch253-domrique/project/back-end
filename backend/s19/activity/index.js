/*
    
    1. Create a function called login which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/
	function login(username, password, role){
		if(username == '' ){
			return "'Inputs must not be empty'";
		}
		if(password == '' ){
			return "'Inputs must not be empty'";
		}
		if(role == '' ){
			return "'Inputs must not be empty'";
		}
			else{
		
				switch (role) {
	            case 'admin':
	                return "Welcome back to the class portal, admin!";
	                break;
	             case 'teacher':
	                return "Thank you for logging in, teacher!";
	                break;
	             case 'rookie':
	                return "Welcome to the class portal, student!";
	                break;   
	            default:
	                return "Role out of range.";
	                break;
	        }

		}
		return role;
	}

	console.log("login()");
	let loginStatementLogin = login("","","");
	console.log(loginStatementLogin);

	let loginStatementUsername = login("","password","admin");
	console.log("login(\"\",\"password\",\"admin\")");
	console.log(loginStatementUsername);

	let loginStatementPassword = login("adminUser","","admin");
	console.log("login(\"adminUser\",\"\",\"admin\")")
	console.log(loginStatementPassword);

	let loginStatementRole = login("adminUser","password","");
	console.log("login(\"adminUser\",\"password\",\"\")");
	console.log(loginStatementPassword);



	console.log("login(\"adminUser\",\"password\",\"admin\")")
	let admin= login("adminUser","password","admin");
	console.log(admin);

	console.log("login(\"teacherUser\",\"password\",\"teacher\")")
	let teacher = login("teacherUser","password","student");
	console.log(teacher);

	console.log("login(\"studentUser\",\"password\",\"student\")")
	let student = login("studentUser","password","student");
	console.log(student);

	console.log("login(\"studentUser\",\"password\",\"carpenter\")")
	let other = login("studentUser","password","carpenter");
	console.log(other);


/*
    2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

    Note: strictly follow the instructed function names.
*/
	function checkAverage(num1, num2, num3, num4) {

		function average(){
			let average = (num1 + num2 + num3 + num4) / 4;
			let roundedAverage;
			roundedAverage = Math.round(average);
			

				if (roundedAverage <= 74){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is F'"

					// }else if (roundedAverage >= 75){
					// 	return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is D'"
					}else if (roundedAverage <= 79){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is D'"

					// }else if (roundedAverage >= 80){
					// 	return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is C'"
					}else if (roundedAverage <= 84){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is C'"

					// }else if (roundedAverage >= 85){
					// 	return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is B'"
					}else if (roundedAverage <= 89){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is B'"

					// }else if (rouAndedAverage <= 90){
					// 	return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is B'"
					}else if (roundedAverage <= 95){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is A'"

					}else if (roundedAverage >= 96){
						return "'Hello, student, your roundedAverage is: " + roundedAverage + ". The letter equivalent is A+'"

					}

		}
		// let average1 = average();
		// console.warn(average1);	
		return average();
		// return average1();
	}
	console.log("checkAverage(71, 70, 73, 71)")
	let result1 = checkAverage(71, 70, 73, 71);
	console.log(result1);

	console.log("checkAverage(75, 75 76, 78)")
	let result2 = checkAverage(75, 75, 76, 78);
	console.log(result2);

	console.log("checkAverage(80, 82, 83, 81)")
	let result3 = checkAverage(80, 82, 83, 81);
	console.log(result3);

	console.log("checkAverage(85, 86, 85, 86)")
	let result4 = checkAverage(85, 86, 85, 86);
	console.log(result4);

	console.log("checkAverage(91, 90, 92, 90)")
	let result5 = checkAverage(91, 90, 92, 90);
	console.log(result5);

	console.log("checkAverage(95, 96, 97, 96)")
	let result6 = checkAverage(95, 96, 97, 96);
	console.log(result6);




//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}
